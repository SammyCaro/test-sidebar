import React from "react";
import "./App.css";
import Table from "./components/table";
/*
import MenuLateral from "./components/Sidebar";
import Login from './components/login';
import {Animated} from "react-animated-css";
import Draggable from 'react-draggable';
import DayPicker from "react-day-picker";
import "react-day-picker/lib/style.css";*/
import ChartComponent from "./components/Charts";
import ChartEditor from "./components/ChartEditor";

export default class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    const { locale } = this.state;
    return (
      <>
        {/*<MenuLateral/>*/}
        <Table />
        <ChartComponent />
        {/*<ChartEditor />*/}
      </>
    );
  }
}
