import React from 'react';
import Sidebar from "react-sidebar";

const Estilos = {
    root: {
        position: "absolute",
        overflow: "hidden",
        
        },
    sidebar: {
        zIndex: 2,
        position: "fixed",
        transition: "transform .3s ease-out",
        willChange: "transform",
        overflowY: "auto",
        background: "white",
        width: "122px",
        textAlign: "center",
        background: "rgba(15, 15, 15, .9)",
        fontFamily: "Georgia, serif",
        letterSpacing: "1.1px",
        color: "white",
    },
}

export default class MenuLateral extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
          sidebarOpen: false
        };
        this.onSetSidebarOpen = this.onSetSidebarOpen.bind(this);
      }
     
      onSetSidebarOpen(open) {
        this.setState({ sidebarOpen: open });
      }
     
    render() {
        return (
        <>
          <Sidebar
            sidebar={
                <div>
                    <div className="header">
                        <b>Menú de filtros</b>
                    </div>
                    <div className="content">
                        <label>Item</label>
                        <br/>
                        <select>
                            <option>
                                15578
                            </option>
                            <option>
                                15278
                            </option>
                        </select>
                        <hr className="linea"/>
                        <br/>
                        <label>Item</label>
                        <br/>
                        <select>
                            <option>
                                15578
                            </option>
                            <option>
                                15278
                            </option>
                        </select>
                        <hr className="linea"/>
                        <br/>
                        <label>Item</label>
                        <br/>
                        <select>
                            <option>
                                15578
                            </option>
                            <option>
                                15278
                            </option>
                        </select>
                        <hr className="linea"/>
                        <br/>
                        <label>Item</label>
                        <br/>
                        <select>
                            <option>
                                15578
                            </option>
                            <option>
                                15278
                            </option>
                        </select>
                    </div>
                </div>
            }
            open={this.state.sidebarOpen}
            onSetOpen={this.onSetSidebarOpen}
            styles={Estilos} 
            docked
          >
            {/*<button 
                onClick={() => this.onSetSidebarOpen(true)}
                className="btnMenu"
            >
              Desplegar menú
            </button>*/}
          </Sidebar>

        </>
        );
    }
}
     


