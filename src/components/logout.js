import React from 'react';
import GoogleLogout from 'react-google-login';

export default function Logout (){

    const onSuccess = () =>{
        alert('Disconnected succesfully');
    }

    return(
        <div>
            <GoogleLogout
                clientId="20642164500-7j78gr8ef1ir2f4b8o9ju7g5qskthrs3.apps.googleusercontent.com"
                buttonText="Cerrar Sesión"
                onLogoutSucess={onSuccess}
            />
        </div>
     )
}