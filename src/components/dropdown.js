import React, { useState } from "react";
import Styled from "styled-components";

/*styles*/
const DropDownContainer = Styled("div")`
    width: 150px;
    font-family: Georgia, serif;
    font-size: 16px;
    position: absolute;
    display: inline-block;

`;
const DropDownHeader = Styled("div")`
    background:#53C024;
    padding:5px;
    font-weight: bold;
    text-align: center;
    color: white;
    border-radius: 5px;
    box-shadow: 1px 1px 4px #53C024;
    border: 1px solid #53C024;
    
`;
const DropDownListContainer = Styled("div")`
    border: 1px solid gray;
    border-radius: 5px;
    box-shadow: 1px 1px 3px gray;
    border-bottom: 1px solid gray;
    color: green;
    overflow: auto;
    z-index: 1;
    
`;
const DropDownList = Styled("ul")`
    list-style:none;

`;
const ListItem = Styled("li")`
    border-bottom: 1px solid gray;
    font-size: 15px;
    padding: 3px;
   

`;

/*End styles*/

const options = ["1865", "5782", "72245", "72245","72245","72245"];

export default function App() {
   
/*Behavior*/
  const [isOpen, setIsOpen] = useState(false);
  const [selectedOption, setSelectedOption] = useState(null);

  const toggling = () => setIsOpen(!isOpen);

  const onOptionClicked = value => () => {
    setSelectedOption(value);
    setIsOpen(false);
    console.log(selectedOption);
  };
    /*End Behavior*/

  return (
      <DropDownContainer>
        <DropDownHeader onClick={toggling}>
          {selectedOption || "Sku"}
        </DropDownHeader>
        {isOpen && (
          <DropDownListContainer>
            <DropDownList>
              {options.map(option => (
                <ListItem onClick={onOptionClicked(option)} key={Math.random()}>
                  {option}
                </ListItem>
              ))}
            </DropDownList>
          </DropDownListContainer>
        )}
      </DropDownContainer>
  );
}