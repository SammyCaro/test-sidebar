import React from 'react';
import DataTable, { createTheme } from 'react-data-table-component';
import Registros from '../registros/data.json';
import Columns from '../registros/columns.json';
import './estilos.css';
import Styled from 'styled-components';

const estilo={
    rows:{
        style: {
            color: "green",
            fontFamily: "Georgia, serif"
        }
    },
    headCells:{
        style:{
            backgroundColor: "#53C024",
            color: "white",
            fontWeight: "bolder",
            padding: "10px 0",
            fontSize: "16px",
        }
    },
    cells: {
        style: {
            padding: "10px",
            fontSize: "13px"
        }
    }
}

const Component = Styled.div`
    width: 1100px;
    margin:auto;
    border-radius: 10px 10px 0 0;
    border-top: 1px solid #dfe0e2;
    border-left: 1px solid #dfe0e2;
    border-right: 1px solid #dfe0e2;
`;

const paginationConfig = {
    rowsPerPageText: 'Registros por página:',
    rangeSeparatorText: 'de',
    selectAllRowsItem: true,
    selectAllRowsItemText: 'Todos'
}

export default class Table extends React.Component{

        state = {
            data: Registros,
            columns: Columns,
        }
    render(){
        return(
            <Component>
                <DataTable
                    title="CCU Demand Planner"
                    columns={this.state.columns}
                    data={this.state.data}
                    pagination
                    paginationComponentOptions={paginationConfig}
                    customStyles={estilo}
                    fixedHeader
                    highlightOnHover
                    //theme="tema1"
                    //selectableRows
                    dense
                />
            </Component>
        )
    }
}