import React from "react";
import { Chart } from "react-google-charts";

const options = {
  chart: {
    title: "Ventas históricas",
    subtitle: "En hectolitros",
  },
};

const data = [
  ["Productos", "2018", "2019", "2020"],
  ["Cervezas", 141, 120, 540],
  ["Jugos", 202, 100, 241],
  ["Néctar", 166, 44, 651],
  ["Licores", 250, 47, 750],
  ["Gaseosas", 274, 100, 544],
];

export default class ChartEditor extends React.Component {
  render() {
    return (
      <>
        <div className="container">
          <button
            onClick={() => {
              const { google, chartEditor, chartWrapper } = this.state;
              if (
                chartWrapper === null ||
                google === null ||
                chartEditor === null
              )
                return;
              chartEditor.openDialog(chartWrapper);
              google.visualization.events.addListener(chartEditor, "ok", () => {
                const newChartWrapper = chartEditor.getChartWrapper();
                newChartWrapper.draw();
              });
            }}
          >
            Editar gráfico
          </button>
          <Chart
            width={"600px"}
            height={"400px"}
            chartType="Line"
            loader={<div>Loading Chart</div>}
            data={data}
            options={options}
            getChartEditor={({ chartEditor, chartWrapper, google }) => {
              this.setState({ chartEditor, chartWrapper, google });
            }}
            chartPackages={["corechart", "controls", "charteditor"]}
            colors={["#e0440e", "#e6693e", "#ec8f6e", "#f3b49f", "#f6c7b6"]}
          />
        </div>
      </>
    );
  }
}
