import React from "react";
import Chart from "react-google-charts";

const data = [
  ["Productos", "2018", "2019", "2020"],
  ["Cervezas", 141, 120, 540],
  ["Jugos", 202, 100, 241],
  ["Néctar", 166, 44, 651],
  ["Licores", 250, 47, 750],
  ["Gaseosas", 274, 100, 544],
];

const options = {
  title: "Venta histórica",
  titleTextStyle: {
    color: "#3f7c20",
    fontSize: 15,
    bold: true,
    fontName: "Georgia, serif",
  },

  backgroundColor: "#f6f7f4",
  selectionMode: "multiple",
  colors: ["#F7B41E", "#0096D6", "#CD1F4C"],
  explorer: {},
  fontSize: 15,

  pointShape: "diamond",
  pointSize: 7,

  legend: {
    position: "right",
    alignment: "center",
    textStyle: {
      color: "#3f7c20",
      fontSize: 16,
      bold: true,
      fontName: "Georgia, serif",
    },
  },

  vAxis: {
    title: "Hectolitro",
    textStyle: { color: "#497f33", fontName: "Georgia, serif" },
  },

  hAxis: {
    title: "Producto",
    textStyle: { color: "#497f33", fontName: "Georgia, serif" },
  },

  series: {
    0: { lineWidth: 3, lineDashStyle: [4, 4] },
    1: { lineWidth: 3, lineDashStyle: [4, 4] },
    2: { lineWidth: 3, lineDashStyle: [4, 4] },
  },

  animation: {
    startup: true,
    easing: "out",
    duration: 2000,
  },

  focusTarget: "category",
  /*crosshair: {
    trigger: "selection",
  },*/
};
export default class ChartComponent extends React.Component {
  render() {
    return (
      <div className="container">
        <Chart
          chartType="LineChart"
          width="80%"
          height="300px"
          data={data}
          options={options}
          /*toolbarItems={[
            {
              type: "csv",
              datasource: data,
            },
          ]}*/
        />
      </div>
    );
  }
}
