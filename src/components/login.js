import React, {useState} from 'react';
import GoogleLogin from 'react-google-login';
import Styled from 'styled-components';

    const BotonGoogle= Styled.button`
    background: rgba(43, 102, 18, .9);
    font-weight: bolder;
    padding: 12.5px;
    width: 150px;
    border-radius: 10px;
    box-shadow: 1.5px 1.5px 4px gray;
    border: 1px solid gray;
    color: white;
    &:hover{
        background: #41941E;
    }
    position: absolute;
    top:0;
    left:76%;
    margin:auto;
    `;

    const LoginProfile = Styled.div`
    width: 20%;
    position: absolute;
    top: 3.5px;
    left: 30%;
    `;

    const Welcome = Styled.p`
    font-weight: bolder;
    color: green;
    float: left;
    margin-right: 3%; 
    `;

    const LoginPic = Styled.img`
    width:40px;
    border-radius: 100%;
    float: left;
    margin-top: 6.4px;
    margin-right: 3%; 
    `;

    const LoginName = Styled.p`
    float: left;
    font-weight: 600;
    color: rgba(43, 102, 18, .9);
    `;

export default function Login(){

    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [url, setUrl] = useState("");
    const [bienvenida, setBienvenida] = useState("");

    const responseGoogle=(response)=>{
        console.log(response);
        console.log("Success!!!");
        setName(response.profileObj.name);
        setEmail(response.profileObj.email);
        setUrl(response.profileObj.imageUrl);
        setBienvenida("Bienvenid@: ");
    };

    const failResponseGoogle=(response)=>{
        console.log(response);
        console.log("Failed!!!");
    }

        return(
            <div>
                <GoogleLogin
                    clientId="20642164500-7j78gr8ef1ir2f4b8o9ju7g5qskthrs3.apps.googleusercontent.com"
                    render={renderProps => (
                    <BotonGoogle onClick={renderProps.onClick}>Sign-in with Google</BotonGoogle>
                    )}
                    buttonText = "Iniciar Sesión"
                    onSuccess={responseGoogle}
                    onFailure={failResponseGoogle}
                    cookiePolicy={'single_host_origin'}
                    isSignedIn={false}
                />

                <LoginProfile>
                    <Welcome>{bienvenida}</Welcome>
                    <LoginPic src={url}/>
                    <LoginName>{name}</LoginName>
                </LoginProfile>
            </div>
        );
}